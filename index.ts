﻿export class Event {
}
export class Command {
}

export abstract class EventListener { abstract handle(event: Event, context: any) }
export abstract class CommandHandler { abstract handle(command: Command, context: any) }

interface EventListenerContainer { listener: any, context: any }
interface CommandHandlerContainer { handler: any, context: any }

export class Cqrs {
    private static _instance: Cqrs;

    public static get Instance() {
        return this._instance || (this._instance = new this());
    }

    public verbose: boolean = false
    private commandHandlers: Array<CommandHandlerContainer>;
    private eventListeners: Array<Array<EventListenerContainer>>;
    constructor() {
        this.commandHandlers = new Array<CommandHandlerContainer>();
        this.eventListeners = new Array<Array<EventListenerContainer>>();
    }
    AddCommandHandler<T extends CommandHandler>(command: Command, commandHandler: { new (): T }, context) {
        if (this.verbose)
            console.log(`Registered command handler ${(commandHandler as any).name} for command ${(command as any).name}`)
        this.commandHandlers[(command as any).name] = { handler: commandHandler, context: context };
    }
    AddEventListener<T extends EventListener>(event: Event, eventListener: { new (): T }, context) {
        let eventName = (event as any).name
        if (this.verbose)
            console.log(`Registered event listener ${(eventListener as any).name} for event ${eventName}`)
        if (!this.eventListeners[eventName]) {
            this.eventListeners[eventName] = new Array();
        }
        this.eventListeners[eventName].push({ listener: eventListener, context: context });
    }
    RemoveCommandHandler(command: Command) {
        if (this.verbose)
            console.log(`Removed command handler for command ${(command as any).name}`)
        this.commandHandlers[(command as any).name] = null;
    }
    RemoveEventListener<T extends EventListener>(event: Event, eventListener: { new (): T }) {
        let eventName = (event as any).name
        if (this.verbose)
            console.log(`Removed event listener ${(eventListener as any).name} for event ${eventName}`)
        var specificEventListeners = this.eventListeners[eventName];
        if (specificEventListeners) {
            for (var i = specificEventListeners.length - 1; i >= 0; i--) {
                if (specificEventListeners[i].listener === eventListener) {
                    specificEventListeners.splice(i, 1);
                }
            }
        }
    }
    Send(command: Command) {
        if (this.verbose)
            console.log(`Sent command ${(command as any).constructor.name} /**/`, command)
        if (!this.commandHandlers[(command.constructor as any).name]) {
            throw "Command " + (command.constructor as any).name + " has no handler.";
        }
        let commandHandlerType = this.commandHandlers[(command.constructor as any).name]
        let commandHandler = new commandHandlerType.handler()
        return commandHandler.handle(command, commandHandlerType.context)
    }
    Publish(event: Event) {
        if (this.verbose)
            console.log(`Published event ${(event as any).constructor.name} /**/`, event)
        if (!this.eventListeners[(event.constructor as any).name])
            return;
        this.eventListeners[(event.constructor as any).name].forEach(function (eventListenerContainer: EventListenerContainer) {
            let eventListener = new eventListenerContainer.listener()
            eventListener.handle(event, eventListenerContainer.context);
        });
    }
};
