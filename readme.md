# Usage

js-cqrs is a lightweight and easy to use implementation of the cqrs pattern. It allows for quick integration due to simplified API.

```

export class SaveUserRequest extends Command{
	data: any;
	constructor(data: any){
			this.data = data;
	}
}

export class SaveUserRequestHandler extends CommandHandler {
	handle(command: SaveUserRequest){
			var event = new UserSaved({firstName: command.data.firstName});
			Cqrs.Instance.Publish(event);
	}
}

export class UserSaved extends Event {
    data: any;
    constructor(data: any) {
        this.data = data;
    }
}

class EventListener1 extends EventListener {
	static processed: boolean = false;
	static firstName: string = "";
	handle(event: UserSaved) {
		EventListener1.processed = true;
		EventListener1.firstName = event.data.firstName;
	}
}

class EventListener2 extends EventListener {
	static processed: boolean = false;
	handle(event: UserSaved): void {
		EventListener2.processed = true;
	}
}

Cqrs.Instance.AddCommandHandler(SaveUserRequest, SaveUserRequestHandler, null);
Cqrs.Instance.AddEventListener(UserSaved, EventListener1, null);
Cqrs.Instance.AddEventListener(UserSaved, EventListener2, null);

let newSaveUserCommand = new SaveUserRequest({ id: '1', firstName: 'Sebastian' });
Cqrs.Instance.Send(newSaveUserCommand);

expect(EventListener1.processed).toBe(true);
expect(EventListener2.processed).toBe(true);
expect(EventListener1.firstName).toBe("Sebastian");

```